﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]

public class GizmosController : MonoBehaviour
{

    private Vector3 screenPoint;
    private Vector3 offset;
	public Collider bound;
	public Collider mask;
	public LuckyController luckyController;
    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
        //Debug.Log("mouse down ne");
    }

    void OnMouseDrag()
    {
        Debug.Log("OnMouseDrag");
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
        transform.position = curPosition;
		if (!mask.bounds.Intersects(bound.bounds))
		{
			luckyController.ShowHistoryNow ();
		}
    }

}
