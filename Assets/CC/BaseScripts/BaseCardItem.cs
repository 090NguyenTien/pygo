﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BaseCardItem : MonoBehaviour
{

	[HideInInspector]
	public BaseCardInfo card;
	[HideInInspector]
	public int indexCard;
	[HideInInspector]
	public Image Value, SmallSuite, BigSuite;
}
