﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LobbyDataHelper : MonoBehaviour {


	public string GetSpriteNameGame(GAMEID _gameID)
	{
		switch (_gameID) {
		case GAMEID.MauBinh:
			return "Mậu Binh";
        case GAMEID.BaiCao:
            return "Bài Cào";
            case GAMEID.XiTo:
			return "Xì Tố";
		case GAMEID.TLMN:
			return "Tiến Lên Miền Nam";
		case GAMEID.Phom:
			return "Phỏm";
            case GAMEID.TLDL:
                return "Tiến Lên Đếm Lá";
        }
		return null;
	}
}
