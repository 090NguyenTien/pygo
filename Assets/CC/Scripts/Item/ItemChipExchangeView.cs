﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemChipExchangeView : MonoBehaviour {

	Text txtCost, txtChip, txtGold;
	Image imgMain;
	Button btnItem;

	onCallBackInt itemClick;

	int Index;

	public void Init(int _index, int _chip, int _gold, onCallBackInt _itemClick)
	{
		txtChip = transform.Find ("TxtValueChip").GetComponent<Text> ();
		txtChip.text = Utilities.GetStringMoneyByLong (_chip);

		txtGold = transform.Find ("TxtValueGold").GetComponent<Text> ();
		txtGold.text = Utilities.GetStringMoneyByLong (_gold);

		imgMain = transform.Find ("ImgIconMain").GetComponent<Image> ();
		imgMain.sprite = DataHelper.GetGoldItem (_index + 1);

		btnItem = GetComponent<Button> ();
		btnItem.onClick.AddListener (ItemOnClick);

		itemClick = _itemClick;

		Index = _index;
	}

	void ItemOnClick()
	{
		itemClick (Index);
	}
}
