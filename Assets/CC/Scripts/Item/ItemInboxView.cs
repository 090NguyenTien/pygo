﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ItemInboxView : MonoBehaviour {

    [SerializeField]
    Text txtTitle, txtFromName, txtCreatedDate, txtCreatedTime;
    [SerializeField]
    Image imgBackground, imgMail;
    [SerializeField]
    Button btnSelect;
    [SerializeField]
    Button btnDelete;

    private Inbox inbox;

    public void Init(Inbox inbox)
    {
        this.inbox = inbox;
        btnSelect.onClick.AddListener(BtnSelectOnClick);
        btnDelete.onClick.AddListener(BtnDeleteOnClick);
    }

    public void Show(int index)
    {
        txtTitle.text = inbox.Title;
        txtFromName.text = inbox.Author;
        txtCreatedDate.text = inbox.CreatedDate.ToString("dd/MM/yyyy");
        txtCreatedTime.text = inbox.CreatedDate.TimeOfDay.ToString();

        //if (index % 2 == 0)
        //    imgBackground.gameObject.SetActive(false);
        //else
        //    imgBackground.gameObject.SetActive(true);
    }

    void BtnSelectOnClick()
    {
        PopupInboxManager.Instance.BtnSelectOnClick(inbox);
    }

    void BtnDeleteOnClick()
    {
        PopupInboxManager.Instance.BtnDeleteOnClick(inbox.Id);
    }
}
