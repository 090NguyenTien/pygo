﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;
using System.Collections.Generic;

public class ItemInApp_GEM_View : MonoBehaviour
{
    [SerializeField]
    Text txtCost, txtChip;
    [SerializeField]
    private Image imgMain;
    Button btnItem;

    onCallBackString itemClick;

    string productID = "";

    [SerializeField]
    private List<Sprite> lstSpriteGold = null;

    public void Init(int _index, string _cost, string _chip, string _productID, onCallBackString _itemClick)
    {


        GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;


        btnItem = GetComponent<Button>();
        btnItem.onClick.AddListener(ItemOnClick);

        productID = _productID;

        txtChip.text = _chip;
        //    txtGold.text = _gold;
        txtCost.text = "$ " + _cost;

        Debug.Log("vO DC DAY ROI------------------------- productID === " + productID);

        //Debug.LogError(_index);
        imgMain.sprite = lstSpriteGold[_index];//DataHelper.GetGoldItem (_index);
                                               //  Debug.Log("vO DC DAY ROI LUON NE-------------------------" + txtChip + "------- " + txtCost);
        imgMain.SetNativeSize();

        itemClick = _itemClick;
    }

    void ItemOnClick()
    {
        itemClick(productID);
    }
}
