﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IOSDisable : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> lstObject = null;

    public bool isShowConfig = false;
    private void Awake()
    {
#if UNITY_IOS

        if (isShowConfig == true) // co lay config
        {
            for (int i = 0; i < lstObject.Count; i++)
            {
                lstObject[i].SetActive(false);
            }
            StartCoroutine(getConfigIOS());
        }
        else
        {
            hideIOSButton();
        }

#endif


    }

    private void showIOSButton()
    {
        for (int i = 0; i < lstObject.Count; i++)
        {
            lstObject[i].SetActive(true);
        }
        Destroy(gameObject);
    }
    private void hideIOSButton()
    {
        for (int i = 0; i < lstObject.Count; i++)
        {
            lstObject[i].SetActive(false);
        }
        Destroy(gameObject);
    }

    private IEnumerator getConfigIOS()
    {
        string url = API.Instance.DOMAIN + "/GameBaiConfig.php";

        WWW service = new WWW(url);
        yield return service;
        if (string.IsNullOrEmpty(service.error) == false) //download error
        {
            yield break;
        }
        string[] configData = service.text.Split('\n');

        GameHelper.dicConfig = new Dictionary<string, string>();
        string[] dataElement;
        bool isShowIOSConfig = false;
        for (int i = 0; i < configData.Length; i++)
        {
            dataElement = configData[i].Split('\t');
            if (dataElement[0] == "ShowIOS")
            {
                isShowIOSConfig = dataElement[1] == "true" ? true : false;
            }
        }

        if (isShowIOSConfig == false)
        {
            hideIOSButton();
        }
        else
        {
            showIOSButton();
        }


    }
}
