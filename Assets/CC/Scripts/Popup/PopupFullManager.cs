﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class PopupFullManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;
	[SerializeField]
	Text txtContent;
	[SerializeField]
	Button btnLeft, btnRight;

	onCallBack leftOnClick, rightOnClick;

	public void Init()
	{
		btnLeft.onClick.AddListener (BtnLeftOnClick);
		btnRight.onClick.AddListener (BtnRightOnClick);
	}

	public void Show(string _title, string _content, onCallBack _btnLeftClick, onCallBack _btnRightOnClick)
	{
		panel.SetActive (true);
//		txtTitle.text = _title;
		txtContent.text = _content;

		leftOnClick = _btnLeftClick;
		rightOnClick = _btnRightOnClick;
	}
	public void Show(string _content, onCallBack _btnLeftClick, onCallBack _btnRightOnClick)
	{
		panel.SetActive (true);
		txtContent.text = _content;

		leftOnClick = _btnLeftClick;
		rightOnClick = _btnRightOnClick;
	}
	public void Hide()
	{
		panel.SetActive (false);
	}

	void BtnLeftOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		leftOnClick ();
	}
	void BtnRightOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		rightOnClick ();
	}

}
