﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemShop : MonoBehaviour {
    [SerializeField]
    Image ImgItem, ImgUse;
    [SerializeField]
    GameObject ImgClock, _DiamonPrice, _ChipPrice;
    [SerializeField]
    Text TxtNameItem, TxtDiamonPrice, TxtChipPrice, TxtCount, TxtPoint;

    [SerializeField]
    MadControll Mad;
    [SerializeField]
    TestItemInHome PanelBuy;

    public long LongDiamonPrice, LongChipPrice, Save_Chip, Save_Gem;

    public string Ten, ThongTin, Id, Img;

    public int DiemTaiSan, stt, soluong;



    public bool IsClock = true;
    public string MyId = "";
    Button MyButton;
    // Use this for initialization





  



    public void Init(string Id, bool Clock, string count, bool isSale = false, long PercentSale = 0)
    {
 
        My_Item it = DataHelper.DuLieuShop[Id];
        MyId = Id;
        IsClock = Clock;
        Img = it.img;

        //     ImgItem.sprite = DataHelper.dictSprite_Shop[Id];

        API.Instance.Load_SprItem(Img, ImgItem);

        TxtNameItem.text = it.ten;
        MyButton = this.gameObject.GetComponent<Button>();
        LongDiamonPrice = it.gem;
        LongChipPrice = it.chip;

        TxtPoint.text = it.diemtaisan.ToString();
        DiemTaiSan = it.diemtaisan;
        ThongTin = it.thongtin;

        Mad = GameObject.Find("Mad").GetComponent<MadControll>();

        if (IsClock == true)
        {

            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();

            GameObject panell = estate.PanelBuyItem;

            PanelBuy = panell.GetComponent<TestItemInHome>();

            // PanelBuy = GameObject.Find("PanelBuyItem").GetComponent<TestItemInHome>();



            TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(LongDiamonPrice);

            if (LongChipPrice == 0)
            {

                _ChipPrice.SetActive(false);
            }
            else
            {
                TxtChipPrice.text = Utilities.GetStringMoneyByLong(LongChipPrice);
                _ChipPrice.SetActive(true);
            }

            
            _DiamonPrice.SetActive(true);

            ImgClock.SetActive(true);

            MyButton.onClick.RemoveAllListeners();
            MyButton.onClick.AddListener(BtnClockClick);
        }
        else
        {
            Mad = GameObject.Find("Mad").GetComponent<MadControll>();
            TxtCount.text = count;
            TxtCount.gameObject.SetActive(true);
            _DiamonPrice.SetActive(false);
            _ChipPrice.SetActive(false);
            ImgClock.SetActive(false);
            ImgUse.gameObject.SetActive(true);
            MyButton.onClick.RemoveAllListeners();

            if (count == "0")
            {
                MyButton.onClick.AddListener(DontChange);
            }
            else
            {
                MyButton.onClick.AddListener(BtnClick);
            }

        }

    }


    public void InitCar(string Id, bool Clock, string count, bool isSale = false, long PercentSale = 0)
    {

        My_Item it = DataHelper.DuLieuXe[Id];

        MyId = Id;
        IsClock = Clock;

        Img = it.img;
        API.Instance.Load_SprItem(Img, ImgItem);
        //  ImgItem.sprite = DataHelper.dictSprite_Car[Id];
        TxtNameItem.text = it.ten;
        MyButton = this.gameObject.GetComponent<Button>();
        LongDiamonPrice = it.gem;
        LongChipPrice = it.chip;
        ThongTin = it.thongtin;

        Mad = GameObject.Find("Mad").GetComponent<MadControll>();
 
        TxtPoint.text = it.diemtaisan.ToString();
        DiemTaiSan = it.diemtaisan;
       
        if (IsClock == true)
        {

            GameObject obj = GameObject.Find("PanelUI_Home");
            EstateControll estate = obj.GetComponent<EstateControll>();
            GameObject panell = estate.PanelBuyItem;
            PanelBuy = panell.GetComponent<TestItemInHome>();

            // PanelBuy = GameObject.Find("PanelBuyItem").GetComponent<TestItemInHome>();
            TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(LongDiamonPrice);
            if (LongChipPrice == 0)
            {
                _ChipPrice.SetActive(false);
            }
            else
            {
                TxtChipPrice.text = Utilities.GetStringMoneyByLong(LongChipPrice);
                _ChipPrice.SetActive(true);
            }
            _DiamonPrice.SetActive(true);

            ImgClock.SetActive(true);
            MyButton.onClick.RemoveAllListeners();
            MyButton.onClick.AddListener(BtnCarClockClick);
        }
        else
        {

            Mad = GameObject.Find("Mad").GetComponent<MadControll>();
          
            TxtCount.text = count;
            TxtCount.gameObject.SetActive(true);
            _DiamonPrice.SetActive(false);
            _ChipPrice.SetActive(false);
            ImgClock.SetActive(false);
            ImgUse.gameObject.SetActive(true);
            MyButton.onClick.RemoveAllListeners();
            if (count == "0")
            {
                MyButton.onClick.AddListener(DontChange);
            }
            else
            {
                MyButton.onClick.AddListener(BtnCarClick);
            }
        }
    }


    public void DontChange()
    {
        Mad.DontChange();
    }


    public void BtnClick()
    {       
        Mad.ChangeShop(MyId);
    }
    public void BtnClockClick()
    {
        // Mad.ChangeHome(MyId);
        PanelBuy.InitShop(MyId, TxtNameItem.text, ThongTin, LongDiamonPrice, LongChipPrice, DiemTaiSan);
        PanelBuy.Show();
    }

    public void BtnCarClick()
    {
        Mad.ChangeCar(MyId);
    }
    public void BtnCarClockClick()
    {
        // Mad.ChangeHome(MyId);
        PanelBuy.InitCar(MyId, TxtNameItem.text, ThongTin, LongDiamonPrice, LongChipPrice, DiemTaiSan);
        PanelBuy.Show();
    }

}
