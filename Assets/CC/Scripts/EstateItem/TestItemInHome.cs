﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class TestItemInHome : MonoBehaviour {
    [SerializeField]
    Text TxtNameItem, TxtInfoItem, TxtDiamonPrice, TxtChipPrice;
    [SerializeField]
    Image ImgItem;
    string MyId = "";
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    EstateControll estate;
    private int ChangeType = -1;

    public long GiaChip, GiaGem;
    [SerializeField]
    GameObject Alert;
    [SerializeField]
    Text TxtAlert, TxtPoint;
    [SerializeField]
    GameObject BtnCHIP;
    [SerializeField]
    ShopBorderAvatar ShopBorder;
    public int Point = 0;

    public void Init(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point)
    {
        MyId = ID;
       // ImgItem.sprite = DataHelper.dictSprite_House[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);

        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }

        TxtPoint.text = point.ToString();
        Point = point;
        ChangeType = 1;




        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }

    public void InitShop(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point)
    {
        MyId = ID;
        //    ImgItem.sprite = DataHelper.dictSprite_Shop[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);

        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }
        ChangeType = 2;
        TxtPoint.text = point.ToString();
        Point = point;

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }


    public void InitCar(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point)
    {
        MyId = ID;
       // ImgItem.sprite = DataHelper.dictSprite_Car[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }
        ChangeType = 3;
        TxtPoint.text = point.ToString();
        Point = point;

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }


    public void InitItemInShop(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point, Sprite spr)
    {
        MyId = ID;
        //ImgItem.sprite = spr;

        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);

        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }
        
        TxtPoint.text = point.ToString();
        Point = point;

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }




    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void BtnBackOnClick()
    {
        this.gameObject.SetActive(false);
        ChangeType = -1;
    }

    public void BtnTestOnClick(string loaitien)
    {
        // this.gameObject.SetActive(false);


        estate.RequestMuaVatPham(MyId, loaitien);


        /*
        if (ChangeType == 1)
        {
           // Mad.ChangeHome(MyId);
          //  Mad.BuyHome(MyId);

            estate.RequestMuaVatPham(MyId, loaitien);
            
        }
        else if (ChangeType == 2)
        {
           
            Mad.ChangeShop(MyId);
            Mad.BuyShop(MyId);
        }
        else if (ChangeType == 3)
        {

            Mad.ChangeCar(MyId);
            Mad.BuyCar(MyId);
        }
        */
    }

    public void ThongBaoLoi(string msg)
    {
        TxtAlert.text = msg;
        Alert.SetActive(true);
    }

    public void MuaBangChip()
    {
        if (MyInfo.CHIP >= GiaChip)
        {
            BtnTestOnClick("chip");
        }
        else
        {
            TxtAlert.text = "Bạn không đủ CHIP mua vật phẩm này";
            Alert.SetActive(true);
        }


    }


    public void MuaBangGem()
    {
        if (MyInfo.GEM >= GiaGem)
        {
            BtnTestOnClick("gem");
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM mua vật phẩm này";
            Alert.SetActive(true);
        }

    }


    public void MuaBangChip_InShop()
    {
        if (MyInfo.CHIP >= GiaChip)
        {
            API.Instance.RequestMuaVatPham(MyId, "chip", RspLayMuaVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ CHIP mua vật phẩm này";
            Alert.SetActive(true);
        }


    }


    public void MuaBangGem_InShop()
    {
        if (MyInfo.GEM >= GiaGem)
        {
            API.Instance.RequestMuaVatPham(MyId, "gem", RspLayMuaVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM mua vật phẩm này";
            Alert.SetActive(true);
        }

    }


    void RspLayMuaVatPham(string _json)
    {
        Debug.LogWarning("DU LIEU mua SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string id = node["asset_id"]["$id"].Value;


        if (sta == "1")
        {
            MyInfo.CHIP = chip;
            MyInfo.GEM = gem;



            if (DataHelper.DuLieuSoHuu.ContainsKey(id))
            {

                DataHelper.DuLieuSoHuu[id] = DataHelper.DuLieuSoHuu[id] + 1;
                DataHelper.KhoItem[id] = DataHelper.DuLieuSoHuu[id];
                Debug.LogWarning("Da Them vao du lieu so huu CU");
            }
            else
            {

                DataHelper.DuLieuSoHuu.Add(id, 1);
                if (DataHelper.KhoItem.ContainsKey(id))
                {
                    DataHelper.KhoItem[id] = 1;
                }
                else
                {
                    DataHelper.KhoItem.Add(id, 1);
                }

                Debug.LogWarning("Da Them moi du lieu so huu");
            }

            int DiemCongThem = DataHelper.DuLieuDiemTaiSan[id];
            DataHelper.MY_DiemTaiSan += DiemCongThem;
            ShopBorder.ThongBaoMuaThanhCong();
            this.gameObject.SetActive(false);
            
        }
        else
        {
            string MSG = node["msg"].Value;
            ThongBaoLoi(MSG);
        }

        Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + id);
    }







    public void CloseAlert()
    {
        TxtAlert.text = "";
        Alert.SetActive(false);
    }

}
