﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentCardControll : MonoBehaviour
{

    [SerializeField]
    PopupCardControll PopupCard;

    // Use this for initialization

    #region MUA CHIP
    public void The10KOnClick()
    {
        PopupCard.Init("10000", 15);
        PopupCard.gameObject.SetActive(true);
    }

    public void The20KOnClick()
    {
        PopupCard.Init("20000", 15);
        PopupCard.gameObject.SetActive(true);
    }

    public void The50KOnClick()
    {
        PopupCard.Init("50000", 15);
        PopupCard.gameObject.SetActive(true);
    }

    public void The100KOnClick()
    {
        PopupCard.Init("100000", 15);
        PopupCard.gameObject.SetActive(true);
    }

    public void The200KOnClick()
    {
        PopupCard.Init("200000", 15);
        PopupCard.gameObject.SetActive(true);
    }

    public void The500KOnClick()
    {
        PopupCard.Init("500000", 15);
        PopupCard.gameObject.SetActive(true);
    }
    #endregion

    #region MUA GEM

    public void The10KOnClick_GEM()
    {
        PopupCard.Init("10000", 20);
        PopupCard.gameObject.SetActive(true);
    }

    public void The20KOnClick_GEM()
    {
        PopupCard.Init("20000", 20);
        PopupCard.gameObject.SetActive(true);
    }

    public void The50KOnClick_GEM()
    {
        PopupCard.Init("50000", 20);
        PopupCard.gameObject.SetActive(true);
    }

    public void The100KOnClick_GEM()
    {
        PopupCard.Init("100000", 20);
        PopupCard.gameObject.SetActive(true);
    }

    public void The200KOnClick_GEM()
    {
        PopupCard.Init("200000", 20);
        PopupCard.gameObject.SetActive(true);
    }

    public void The500KOnClick_GEM()
    {
        PopupCard.Init("500000", 20);
        PopupCard.gameObject.SetActive(true);
    }


    #endregion



}
