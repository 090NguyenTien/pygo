﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemResultHorseBet : MonoBehaviour {
    [SerializeField]
    Text TxtSlot, TxtBet, TxtWin, TxtRatio;
    [SerializeField]
    Image imgBgWin;
    string slot;

    public string Slot
    {
        get
        {
            return slot;
        }

        set
        {
            slot = value;
        }
    }
    private void Awake()
    {
        
    }
    // Use this for initialization
    public void Init(string slot, int ratio)
    {
        this.slot = slot;
        TxtSlot.text = slot;
        TxtRatio.text = "x"+ratio.ToString();
        imgBgWin.gameObject.SetActive(false);
        gameObject.SetActive(false);
        TxtBet.text = "0";
        //Hide();
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void ClearData()
    {
        TxtBet.text = "0";
        TxtWin.text = "0";
        imgBgWin.gameObject.SetActive(false);
        Hide();
    }
    public void setDataBySlot(long chipBet, long chipWin)
    {
        TxtBet.text = chipBet.ToString();
        TxtWin.text = chipWin.ToString();
        gameObject.SetActive(true);
        if(chipWin>0) imgBgWin.gameObject.SetActive(true);
    }
    public void ShowHistoryBet(string slot, long chipBet, long chipWin, int ratio)
    {
        TxtBet.text = chipBet.ToString();
        TxtWin.text = chipWin.ToString();
        gameObject.SetActive(true);
        TxtSlot.text = slot;
        TxtRatio.text = "x"+ratio.ToString();
        if (chipWin > 0) imgBgWin.gameObject.SetActive(true);
    }
}
