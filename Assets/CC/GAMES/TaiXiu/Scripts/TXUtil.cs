﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class TXUtil
{
    public static string ConvertMoneyToText(long money)
    {
        // Add a comma in the thousands place for a number
        return string.Format("{0:n0}", money);
    }
    public static string ConvertBetTypeToStringBauCua(int betType)
    {
        switch (betType)
        {
            case 1: return "CÁ";
            case 2: return "TÔM";
            case 3: return "BẦU";
            case 4: return "NAI";
            case 5: return "CUA";
            case 6: return "GÀ";
            case 15: return "CÁ:CUA";
            case 42: return "NAI:TÔM";
            case 13: return "CÁ:BẦU";
            case 46: return "NAI:GÀ";
            case 35: return "BẦU:CUA";
            case 62: return "GÀ:TÔM";
        }
        return "";
    }
    public static string ConvertBetTypeToString(int betType)
    {
        switch (betType)
        {
            case -1: return "XỈU";
            case 0: return "TÀI";
            case 1: return "XÍ NGẦU 1";
            case 2: return "XÍ NGẦU 2";
            case 3: return "XÍ NGẦU 3";
            case 4: return "XÍ NGẦU 4";
            case 5: return "XÍ NGẦU 5";
            case 6: return "XÍ NGẦU 6";
            case 7: return "TỔNG 4";
            case 8: return "TỔNG 5";
            case 9: return "TỔNG 6";
            case 10: return "TỔNG 7";
            case 11: return "TỔNG 8";
            case 12: return "TỔNG 9";
            case 13: return "TỔNG 10";
            case 14: return "TỔNG 11";
            case 15: return "TỔNG 12";
            case 16: return "TỔNG 13";
            case 17: return "TỔNG 14";
            case 18: return "TỔNG 15";
            case 19: return "TỔNG 16";
            case 20: return "TỔNG 17";
        }

        return "";
    }

    public static int ToClientBetType(int betType)
    {
        if (betType <= 6)
            return betType;
        return betType - 7;
    }

    public static int ToServerBetType(int betType)
    {
        if (betType <= 6)
            return betType;
        return betType + 7;
    }

    public static long ConvertShortTextToMoney(string text)
    {
        string digits = new String(text.TakeWhile(Char.IsDigit).ToArray());
        char lastLetter = text.Last();
        long moneyValue = long.Parse(digits);
        long moneyRatio = 0;

        switch (lastLetter)
        {
            case 'K': moneyRatio = 1000; break;
            case 'M': moneyRatio = 1000000; break;
            case 'B': moneyRatio = 1000000000; break;
            default: moneyRatio = 0; break;
        }

        return moneyValue * moneyRatio;
    }

    public static string ConvertMoneyToShortText(long money)
    {
        long value = 0;
        string postFix = string.Empty;

        if (money / 1000000000 > 0)
        {
            value = money / 1000000000;
            postFix = "B";
        }
        else if (money / 1000000 > 0)
        {
            value = money / 1000000;
            postFix = "M";
        }
        else if (money / 1000 > 0)
        {
            value = money / 1000;
            postFix = "K";
        }
        return value + postFix;
    }

    public static int[] ConvertStringToDiceResult(string strDiceResult)
    {
        int[] result = new int[3];
        List<string> numbers = strDiceResult.Split(DelimiterKey.underscore).ToList();
        result[0] = int.Parse(numbers[0]);
        result[1] = int.Parse(numbers[1]);
        result[2] = int.Parse(numbers[2]);
        return result;
    }

    public static int ConvertBetMoneyToSpriteIndex(long betMoney)
    {
        switch (betMoney)
        {
            case TaiXiuConst.BET_MONEY_3: return 0;
            case TaiXiuConst.BET_MONEY_4: return 1;
            case TaiXiuConst.BET_MONEY_5: return 2;
            case TaiXiuConst.BET_MONEY_6: return 3;
            //case TaiXiuConst.BET_MONEY_7: return 4;
            case TaiXiuConst.BET_MONEY_9: return 4;
            case TaiXiuConst.BET_MONEY_10: return 5;
            case TaiXiuConst.BET_MONEY_11: return 6;
                //case TaiXiuConst.BET_MONEY_1: return 0;
                //case TaiXiuConst.BET_MONEY_2: return 1;
                //case TaiXiuConst.BET_MONEY_3: return 2;
                //case TaiXiuConst.BET_MONEY_4: return 3;
                //case TaiXiuConst.BET_MONEY_5: return 4;
                //case TaiXiuConst.BET_MONEY_6: return 5;
                //case TaiXiuConst.BET_MONEY_7: return 6;
                //case TaiXiuConst.BET_MONEY_8: return 7;
        }
        return -1;
    }
    public static int ConvertBetMoneyToSpriteIndexBauCua(long betMoney)
    {
        switch (betMoney)
        {
            case TaiXiuConst.BET_MONEY_3: return 0;
            case TaiXiuConst.BET_MONEY_4: return 1;
            case TaiXiuConst.BET_MONEY_5: return 2;
            case TaiXiuConst.BET_MONEY_6: return 3;
            case TaiXiuConst.BET_MONEY_7: return 4;
            case TaiXiuConst.BET_MONEY_9: return 5;
            case TaiXiuConst.BET_MONEY_10: return 6;
            case TaiXiuConst.BET_MONEY_11: return 7;
        }
        return -1;
    }
    public static long[] ConvertMoneyToNumOfAllChipTypes(long money)
    {
        long[] result = new long[8];

        result[7] = money / TaiXiuConst.BET_MONEY_8;
        money -= result[7] * TaiXiuConst.BET_MONEY_8;

        if (result[7] > 5) result[7] = 5;

        result[6] = money / TaiXiuConst.BET_MONEY_7;
        money -= result[6] * TaiXiuConst.BET_MONEY_7;

        result[5] = money / TaiXiuConst.BET_MONEY_6;
        money -= result[5] * TaiXiuConst.BET_MONEY_6;

        result[4] = money / TaiXiuConst.BET_MONEY_5;
        money -= result[4] * TaiXiuConst.BET_MONEY_5;

        result[3] = money / TaiXiuConst.BET_MONEY_4;
        money -= result[3] * TaiXiuConst.BET_MONEY_4;

        result[2] = money / TaiXiuConst.BET_MONEY_3;
        money -= result[2] * TaiXiuConst.BET_MONEY_3;

        result[1] = money / TaiXiuConst.BET_MONEY_2;
        money -= result[1] * TaiXiuConst.BET_MONEY_2;

        result[0] = money / TaiXiuConst.BET_MONEY_1;
        money -= result[0] * TaiXiuConst.BET_MONEY_1;

        return result;
    }

    public static string ConvertIndexToChipText(int index)
    {
        switch (index)
        {
            case 0: return "1K";
            case 1: return "5K";
            case 2: return "10K";
            case 3: return "50K";
            case 4: return "100K";
            case 5: return "500K";
            case 6: return "1M";
            case 7: return "5M";
        }
        return string.Empty;
    }

    public static Dictionary<int, long> ConvertStringToBets(string strBets)
    {
        Dictionary<int, long> bets = new Dictionary<int, long>();
        List<string> betDetails = strBets.Split(DelimiterKey.dollarPack).ToList();

        foreach (string betDetail in betDetails)
        {
            List<string> parts = betDetail.Split(DelimiterKey.colon).ToList();
            int type = int.Parse(parts[0]);
            long money = long.Parse(parts[1]);
            bets.Add(type, money);
        }

        return bets;
    }
    
    public static long GetDifferenceMoney(Dictionary<int, long> oldDic, Dictionary<int, long> curDic, int type)
    {
        long oldMoney = 0;
        long curMoney = 0;

        foreach (KeyValuePair<int, long> pair in oldDic)
            if (pair.Key == type)
                oldMoney = pair.Value;

        foreach (KeyValuePair<int, long> pair in curDic)
            if (pair.Key == type)
                curMoney = pair.Value;

        return curMoney - oldMoney;
    }
}
