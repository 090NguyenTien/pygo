﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class TaiXiuHistoryManager : MonoBehaviour {

    private static TaiXiuHistoryManager instance;
    public static TaiXiuHistoryManager Instance { get { return instance; } }
    [SerializeField]
    GameObject Panel;
    [SerializeField]
    GameObject HistoryList;
    [SerializeField]
    GameObject HistoryItemObj;
    [SerializeField]
    Transform HistoryItemParent;
    [SerializeField]
    Text Message;
    [SerializeField]
    Image Dice1, Dice2, Dice3;
    [SerializeField]
    Text TotalBet, TotalGet;

    private int[] DiceResult;
    private string GameResult;

    void Awake()
    {
        instance = this;
    }

    void ShowItem(int index, HistoryItem item)
    {
        HistoryItemView itemView;
        GameObject obj = Instantiate(HistoryItemObj) as GameObject;
        obj.transform.SetParent(HistoryItemParent);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<HistoryItemView>();
        itemView.Init(item);
        itemView.Show();
    }

    public void Show()
    {
        HistoryList.SetActive(false);
        Panel.SetActive(true);
        Message.text = string.Empty;

        if (DiceResult == null)
        {
            Message.text = "CHƯA XONG VÒNG ĐẦU TIÊN.";
            return;
        }
        
        Dice1.sprite = DiceSprites.Instance.Get(DiceResult[0] - 1);
        Dice2.sprite = DiceSprites.Instance.Get(DiceResult[1] - 1);
        Dice3.sprite = DiceSprites.Instance.Get(DiceResult[2] - 1);

        if (string.IsNullOrEmpty(GameResult))
        {
            Message.text = "BẠN KHÔNG CHƠI VÒNG NÀY.";
            return;
        }

        if (DiceResult[0] == DiceResult[1] && DiceResult[1] == DiceResult[2])
        {
            Message.text = "XỔ BÃO.";
            return;
        }

        HistoryList.SetActive(true);

        for (int i = 0; i < HistoryItemParent.childCount; i++)
            Destroy(HistoryItemParent.GetChild(i).gameObject);

        long totalBet = 0;
        long totalGet = 0;

        string detailResult = GameResult.Split(DelimiterKey.hash).ToList()[0];
        string[] betResults = detailResult.Split(DelimiterKey.dollarPack);
        Dictionary<int, HistoryItem> resultDic = new Dictionary<int, HistoryItem>();

        foreach (string betResult in betResults)
        {
            string[] parts = betResult.Split(DelimiterKey.colon);
            int clientBetType = TXUtil.ToClientBetType(int.Parse(parts[0]));
            HistoryItem item = new HistoryItem();
            item.Type = TXUtil.ConvertBetTypeToString(clientBetType);
            item.Bet = TXUtil.ConvertMoneyToText(long.Parse(parts[1]));
            item.Get = TXUtil.ConvertMoneyToText(long.Parse(parts[2]));
            resultDic.Add(clientBetType, item);
            totalBet += long.Parse(parts[1]);
            totalGet += long.Parse(parts[2]);
        }

        int index = -1;

        foreach (KeyValuePair<int, HistoryItem> pair in resultDic.OrderBy(i => i.Key))
        {
            index++;
            ShowItem(index, pair.Value);
        }

        TotalBet.text = TXUtil.ConvertMoneyToText(totalBet);
        TotalGet.text = TXUtil.ConvertMoneyToText(totalGet);
    }

    public void Hide()
    {
        Panel.SetActive(false);
    }

    public void UpdateHistory(int[] diceResult, string gameResult)
    {
        DiceResult = diceResult;
        GameResult = gameResult;

        if (Panel.activeSelf)
            Show();
    }
}

public class HistoryItem
{
    public string Type { get; set; }
    public string Bet { get; set; }
    public string Get { get; set; }
}